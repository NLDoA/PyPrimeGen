// Initialize variables
var checkthis = 1;
var foundmultiple = true;
var intprime = 1;
var realprime = 1;
var primex = 3;

// Main loop to search for prime numbers
do
	{
// Starting with 1, check for primes by comparing integer division by division
	checkthis = 1;

	do
		{
// Increment the number to check for prime
		checkthis ++;
		intprime = Math.round(primex/checkthis);
		realprime = (primex/checkthis);
//		This is the debugging line (below), it slows down the script by a LOT, and is ONLY suggested for debugging purposes
//		console.log("Is ", primex, "a prime? ","Checking ", checkthis, "***intprime=", intprime, "  realprime=", realprime);

// Check for prime
		if (intprime == realprime)
			{
			foundmultiple = true;
			}
		else
			{
			foundmultiple = false;
			}
		} while ((checkthis < (primex-1))&&(foundmultiple == false));
// If reached the end and foundmultiple = false, then we have a prime
	if ((checkthis == (primex-1))&&(foundmultiple == false))
		{
		console.log("Woohoo ",primex, " is a prime number!");
		}

		primex ++;
	} while (checkthis > -1)
